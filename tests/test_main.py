import textwrap
from typing import List

import pytest


class Solution:
    def split_lists(self, s: str, numRows: int) -> List[List[str]]:
        sub_lists: List[List[str]] = [[] for i in range(numRows)]
        for i in range(len(s)):
            sub_lists[i % numRows].append(s[i])
        return sub_lists

    def concatenate(self, sub_lists: List[List[str]]) -> str:
        result = ""
        for sub_list in sub_lists:
            substr = "".join(sub_list)
            result += substr
        return result

    def convert(self, s: str, numRows: int) -> str:
        if len(s) <= numRows:
            sub_lists = self.split_lists(s, numRows)
            result = self.concatenate(sub_lists)
            return result
        if numRows == 1:
            return self.one_row(s)
        elif numRows == 2:
            return self.two_row(s)
        elif numRows == 3:
            return self.three_row(s)

    def one_row(self,s):
        return s

    def two_row(self, s):
        sublists = [ [], [] ]
        for i, character in enumerate(s):
            sublists[i % 2].append(character)
        return self.concatenate(sublists)
    
    def three_row(self, s):
        sublists = [[],[],[]]
        for i, character in enumerate(s):
            if i % 4 == 3:# and i // 3 == 1:
                sublists[1].append(character)
            else:
                sublists[i % 3].append(character)
        return self.concatenate(sublists)


def strip_whitespace(raw_input):
    return raw_input.replace(" ", "").replace("\n", "")

def test_convert_expected():
    raw_input = """
    a    g
    b  f h  l
    c e  i k
    d    j
    """
    final_expected = "agbfhlceikdj"
    assert strip_whitespace(raw_input) == final_expected


def test_convert_zigzag():
    result = Solution().convert("a", 1)
    assert result == strip_whitespace("a")

def test_one_row():
    result = Solution().convert("abc",1)
    assert result == "abc"

def test_convert_zigzag_abc():
    result = Solution().convert("abc", 2)
    assert result == strip_whitespace(
    """
    a c
    b
    """
    )

def test_convert_abcdef_two_row():
    result = Solution().convert("abcdef", 2)
    assert result == strip_whitespace(
        """
        a c e
        b d f
        """
    )

def test_convert_zigzag_four_characters_three_rows():
    result = Solution().convert("abcd", 3)
    assert result == strip_whitespace(
    """
    a
    b  d
    c
    """
    )
    

def test_convert_zigzag_six_characters_three_rows():
    result = Solution().convert("abcdef", 3)
    assert result == strip_whitespace(
    """
    a     e
    b  d  f
    c     
    """
    )
    
